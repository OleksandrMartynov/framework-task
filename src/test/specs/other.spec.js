import page from '../../po/pages/page.factory.js';
import TestDataReader from '../../po/test-data/test-data-reader.js';

describe('Other test', () => {
    it('other test', async () => {
        const text = 'Google Cloud Pricing Calculator';
        await page('main')
            .open();
        await page('main')
            .header
            .searchButton
            .click();
        await page('main')
            .header
            .searchInput
            .setValue(text);
        await page('main')
            .header
            .startSearchButton
            .click();
        await page('search-result')
            .resultLinkList
            .item(text)
            .click();
        await page('calculator')
            .costDetails
            .addEstimateButton
            .click();
        await page('calculator')
            .productTypeModal
            .item('compute engine')
            .click();
        await page('calculator')
            .computeEngineSettings
            .input('instance count')
            .setValue(TestDataReader.getTestData('INSTANCES_COUNT'));
        await page('calculator')
            .computeEngineSettings
            .select('machine type')
            .click();
        await page('calculator')
            .computeEngineSettings
            .machineTypeList
            .option(TestDataReader.getTestData('INSTANCE_TYPE'))
            .click();
        await page('calculator')
            .computeEngineSettings
            .addGpuButton
            .click();
        await page('calculator')
            .computeEngineSettings
            .select('gpu model')
            .click();
        await page('calculator')
            .computeEngineSettings
            .gpuModelList
            .option(TestDataReader.getTestData('GPU_TYPE'))
            .click();
        await page('calculator')
            .computeEngineSettings
            .select('gpu number')
            .click();
        await page('calculator')
            .computeEngineSettings
            .gpuNumberList
            .option(TestDataReader.getTestData('GPU_COUNT'))
            .click();
        await page('calculator')
            .computeEngineSettings
            .select('local ssd')
            .click();
        await page('calculator')
            .computeEngineSettings
            .localSsdList
            .option(TestDataReader.getTestData('LOCAL_SSD'))
            .click();
        await page('calculator')
            .computeEngineSettings
            .select('region')
            .click();
        await page('calculator')
            .computeEngineSettings
            .regionList
            .option(TestDataReader.getTestData('DATACENTRE'))
            .click();
        await page('calculator')
            .computeEngineSettings
            .input('1-year commitment')
            .click();
        await browser.pause(3000);
        expect(await page('calculator')
                .costDetails
                .estimateCostLabel
                .getText())
            .toEqual(TestDataReader.getTestData('EXPECTED_COST')
        )
    });
});