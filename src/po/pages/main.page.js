import BasePage from './base.page.js';

export default class MainPage extends BasePage {
    constructor(){
        super('https://cloud.google.com/');
    }
}