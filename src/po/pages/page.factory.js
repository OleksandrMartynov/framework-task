import CalculatorPage from './calculator.page.js';
import MainPage from './main.page.js';
import SearchResultPage from './search-result.page.js';

export default function page(name){
    const items = {
        'main':new MainPage(),
        'calculator':new CalculatorPage(),
        'search-result':new SearchResultPage()
    };
    return items[name.toLowerCase()];
}