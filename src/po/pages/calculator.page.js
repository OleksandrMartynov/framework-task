import ComputeEngineSettingsComponent from '../components/calculator/compute-engine/compute-engine-settings.component.js';
import CostDetailsComponent from '../components/calculator/cost-details.component.js';
import ProductTypeModalComponent from '../components/calculator/product-type-modal.component.js';
import ShareModalComponent from '../components/calculator/share-modal.component.js';
import BasePage from './base.page.js';

export default class CalculatorPage extends BasePage{
    constructor(){
        super('https://cloud.google.com/products/calculator');
        this.computeEngineSettings = new ComputeEngineSettingsComponent();
        this.productTypeModal = new ProductTypeModalComponent();
        this.costDetails = new CostDetailsComponent();
        this.shareModal = new ShareModalComponent();
    }
}