import HeaderComponent from '../components/common/header.component.js';

export default class BasePage{
    constructor(url){
        this.url = url;
        this.header = new HeaderComponent();
    }

    async open(){
        return browser.url(this.url);
    }
}