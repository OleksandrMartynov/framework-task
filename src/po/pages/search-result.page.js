import ResultLinkListComponent from '../components/search-result/result-link-list.component.js';
import BasePage from './base.page.js';

export default class SearchResultPage extends BasePage{
    constructor(){
        super('https://cloud.google.com/search');
        this.resultLinkList = new ResultLinkListComponent();
    }
}