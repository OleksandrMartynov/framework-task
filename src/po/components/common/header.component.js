import BaseComponent from './base.component.js';

export default class HeaderComponent extends BaseComponent{
    constructor(){
        super('header');
    }

    get searchButton(){
        return this.rootEl.$('div[class="YSM5S"]');
    }

    get searchInput(){
        return this.rootEl.$('#i4');
    }

    get startSearchButton(){
        return this.rootEl.$('i[aria-label="Search"]');
    }
}