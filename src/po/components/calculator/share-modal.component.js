import BaseComponent from '../common/base.component.js';

export default class ShareModalComponent extends BaseComponent{
    constructor(){
        super('div[aria-label="Share Estimate Dialog"]');
    }

    get copyLinkButton(){
        return this.rootEl.$('//button[contains(text(), "Copy link")]');
    }
}