import BaseComponent from '../../common/base.component.js';

export default class GpuModelListComponent extends BaseComponent{
    constructor(){
        super('ul[aria-label="GPU Model"]');
    }

    option(name){
        const items = {
            'nvidia-v100':'//li[.//span[text()="NVIDIA V100"]]',
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }
}