import BaseComponent from '../../common/base.component.js';

export default class MachineTypeListComponent extends BaseComponent{
    constructor(){
        super('ul[aria-label="Machine type"]');
    }

    option(name){
        const items = {
            'n1-standard-8':'//li[.//span[text()="n1-standard-8"]]',
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }
}