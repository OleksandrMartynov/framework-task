import BaseComponent from '../../common/base.component.js';

export default class LocalSsdListComponent extends BaseComponent{
    constructor(){
        super('ul[aria-label="Local SSD"]');
    }

    option(name){
        const items = {
            '2x375':'//li[.//span[text()="2x375 GB"]]',
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }
}