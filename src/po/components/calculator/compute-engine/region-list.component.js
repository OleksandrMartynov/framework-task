import BaseComponent from '../../common/base.component.js';

export default class RegionListComponent extends BaseComponent{
    constructor(){
        super('ul[aria-label="Region"]');
    }

    option(name){
        const items = {
            'europe-west4':'//li[.//span[contains(text(), "europe-west4")]]'
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }
}