import BaseComponent from '../../common/base.component.js';

export default class GpuNumberListComponent extends BaseComponent{
    constructor(){
        super('ul[aria-label="Number of GPUs"]');
    }

    option(name){
        const items = {
            '1':'//li[.//span[text()="1"]]',
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }
}