import BaseComponent from '../../common/base.component.js';
import GpuModelListComponent from './gpu-model-list.component.js';
import GpuNumberListComponent from './gpu-number-list.component.js';
import LocalSsdListComponent from './local-ssd-list.component.js';
import MachineTypeListComponent from './machine-type-list.component.js';
import RegionListComponent from './region-list.component.js';

export default class ComputeEngineSettingsComponent extends BaseComponent{
    constructor(){
        super('#ucj-1');
        this.gpuModelList = new GpuModelListComponent();
        this.machineTypeList = new MachineTypeListComponent();
        this.localSsdList = new LocalSsdListComponent();
        this.gpuNumberList = new GpuNumberListComponent();
        this.regionList = new RegionListComponent();
    }

    input(name){
        const items = {
            'instance count':'input[type="number"][min="0"][max="50000"]',
            '1-year commitment':'label[for="1-year"]'
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }

    select(name){
        const items = {
            'machine type':'//div[@jsname="wSASue"][.//span[text()="Machine type"]]',
            'gpu model':'//div[@jsname="wSASue"][.//span[text()="GPU Model"]]',
            'local ssd':'//div[@jsname="wSASue"][.//span[text()="Local SSD"]]',
            'gpu number':'//div[@jsname="wSASue"][.//span[text()="Number of GPUs"]]',
            'region':'//div[@jsname="wSASue"][.//span[text()="Region"]]'
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }

    get addGpuButton(){
        return this.rootEl.$('button[aria-label="Add GPUs"]');
    }
}