import BaseComponent from '../common/base.component.js';

export default class ProductTypeModalComponent extends BaseComponent{
    constructor(){
        super('div[aria-label="Add to this estimate"]');
    }

    item(name){
        const items = {
            'compute engine':'//div/h2[text()="Compute Engine"]',
        };
        return this.rootEl.$(items[name.toLowerCase()]);
    }
}