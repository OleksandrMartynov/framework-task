import BaseComponent from '../common/base.component.js';

export default class CostDetailsComponent extends BaseComponent{
    constructor(){
        super('div[class="C7J75c "]');
    }

    get addEstimateButton(){
        return this.rootEl.$('//button/span[text()="Add to estimate"]');
    }

    get shareButton(){
        return this.rootEl.$('//button/span[text()="Share"]');
    }

    get estimateCostLabel(){
        return this.rootEl.$('label.gt0C8e.MyvX5d.D0aEmf');
    }
}