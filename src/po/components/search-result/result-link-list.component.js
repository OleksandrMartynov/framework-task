import BaseComponent from '../common/base.component.js';

export default class ResultLinkListComponent extends BaseComponent{
    constructor(){
        super('div[jsname="J6ONMd"]')
    }

    item(text){
        return this.rootEl.$('//a[contains(text(),\'' + text + '\')]');
    }
}