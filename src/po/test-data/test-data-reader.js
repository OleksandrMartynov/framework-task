import dotenv from 'dotenv';

dotenv.config();

export default class TestDataReader{
    static getTestData(key){
        return process.env[key];
    }
}